__author__ = """OceanProtocol"""
__version__ = '0.2.3'
#  Copyright 2018 Ocean Protocol Foundation
#  SPDX-License-Identifier: Apache-2.0

from oceandb_elasticsearch_driver import plugin
