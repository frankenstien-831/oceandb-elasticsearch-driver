#  Copyright 2018 Ocean Protocol Foundation
#  SPDX-License-Identifier: Apache-2.0

price = "service.attributes.main.price"
license = "service.attributes.main.license"
sample = "service.attributes.additionalInformation.links.type"
categories = "service.attributes.additionalInformation.categories"
tags = "service.attributes.additionalInformation.tags"
created = "created"
datePublished = "service.attributes.main.datePublished"
dateCreated = "service.attributes.main.dateCreated"
updated_frequency = "service.attributes.additionalInformation.updateFrequency"
service_type = "service.type"
metadata_type = "service.attributes.main.type"
name = "service.attributes.main.name"
description = "service.attributes.additionalInformation.description"

list_indexes = [price, license, sample, categories, created, updated_frequency,
                service_type, metadata_type, name, description]
